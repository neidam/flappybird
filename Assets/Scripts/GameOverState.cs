﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverState : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject.FindGameObjectWithTag("scoreGameOver").GetComponent<Text>().text = GameState.Instance.GetScore().ToString();
        GameState.Instance.ResetScore();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("space") || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            GameState.Instance.LoadLevel("Scene2-Menu");
        }
	}
}
