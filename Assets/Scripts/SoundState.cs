﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundState : MonoBehaviour
{

    public static SoundState Instance;

    public AudioClip impulseSound;
    public AudioClip menuMusic;

    private AudioSource music;

    // Use this for initialization
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(Instance.gameObject);
            SceneManager.sceneLoaded += OnSceneLoaded;

            music = GetComponent<AudioSource>();
            music.clip = menuMusic;
            music.spatialBlend = 0;
            music.loop = true;
            music.Play();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void playImpulse()
    {
        MakeSound(impulseSound);
    }

    public void MakeSound(AudioClip originalClip)
    {
        AudioSource.PlayClipAtPoint(originalClip, transform.position);
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        switch (scene.buildIndex)
        {
            case 0:
                music.clip = menuMusic;
                music.Play();
                break;
            case 1:
                break;
            case 2:
                music.Stop();
                break;
            case 3:
                music.clip = menuMusic;
                music.Play();
                break;
        }
    }
}