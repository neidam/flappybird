﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndAction : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Destroy(GetComponent<TouchAction>());
        Destroy(GetComponent<CollideManagementBird>());
        GetComponent<Rigidbody2D>().angularVelocity = -270;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 bottomLeftCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));

        float spriteSizeY = GetComponent<SpriteRenderer>().bounds.size.y;

        if (transform.position.y + spriteSizeY < bottomLeftCorner.y)
        {
            GameState.Instance.LoadLevel("Scene4-GameOver");
            Destroy(gameObject);
        }
	}
}
